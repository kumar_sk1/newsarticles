package com.codesk.newsarticles.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.codesk.newsarticles.R
import com.codesk.newsarticles.model.ArticleList
import com.codesk.newsarticles.utils.DateUtils.convertUTCtoDate
import kotlinx.android.synthetic.main.list_item.view.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class ArticleListAdapter(
    private val items: MutableLiveData<ArticleList>,
    private val context: Context
) : RecyclerView.Adapter<ArticleListAdapter.ArticleListViewHolder>() {

    private val TAG = ArticleListAdapter::class.java.canonicalName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleListViewHolder {
        Log.d(TAG,"onCreateViewHolder()")
        return ArticleListViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        Log.d(TAG,"getItemCount()")
        return if (items.value?.articles != null) {
            items.value!!.articles!!.size
        } else {
            0
        }
    }

    override fun onBindViewHolder(holder: ArticleListViewHolder, position: Int) {
        val title = items.value?.articles!![position].title
        val description = items.value?.articles!![position].description
        val source = items.value?.articles!![position].source?.name
        val publishedDate = convertUTCtoDate(items.value?.articles!![position].publishedAt)
        if (title != null) {
            holder.title.text = title
        } else {
            holder.title.text = context.resources.getString(R.string.no_title)
        }
        if (description != null) {
            holder.description.text = description
        } else {
            holder.description.text = context.resources.getString(R.string.no_description)
        }

        if (source != null) {
            holder.source.text = source
        } else {
            holder.source.text = context.resources.getString(R.string.no_source)
        }

        holder.publishedDate.text = "Date: $publishedDate"

        var url: String? = items.value?.articles!![position].urlToImage.toString()
        if (url != null) {
            if (url.startsWith("http://")) {
                url = url.replace("http://", "https://")
            }
            Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE).apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.default_poster)
                .into(holder.poster)
        }
    }


    class ArticleListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.title
        val description: TextView = view.description
        val source: TextView = view.source
        val publishedDate: TextView = view.publishedDate
        val poster: ImageView = view.poster
    }
}