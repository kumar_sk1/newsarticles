package com.codesk.newsarticles.restservice

import com.codesk.newsarticles.NewsArticles
import com.codesk.newsarticles.utils.Constants.BASE_URL
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    private const val cacheSize = (5 * 1024 * 1024).toLong()
    private val myCache = Cache(NewsArticles.context.cacheDir, cacheSize)
    private var maxAge = 60*60  // read from cache for 1 hour

    private val okHttpClient : OkHttpClient by lazy {
        val interceptor = HttpLoggingInterceptor()
        val builder = OkHttpClient.Builder()

        builder.addInterceptor(interceptor.setLevel(HttpLoggingInterceptor.Level.BODY))
            .cache(myCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = request.newBuilder().header("Cache-Control", "public, max-age=$maxAge").build()
                chain.proceed(request)
            }
            .build()
    }

    fun retrofit() : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}