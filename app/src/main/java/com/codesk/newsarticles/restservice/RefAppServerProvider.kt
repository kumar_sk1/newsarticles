package com.codesk.newsarticles.restservice

object RefAppServerProvider {
    val refApi : RefAPIServices = RetrofitService.retrofit()
        .create(RefAPIServices::class.java)
}