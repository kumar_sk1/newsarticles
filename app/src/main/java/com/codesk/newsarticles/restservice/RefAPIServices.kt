package com.codesk.newsarticles.restservice

import com.codesk.newsarticles.model.ArticleList
import retrofit2.Response
import retrofit2.http.GET

interface RefAPIServices {
    @GET("Android/news-api-feed/staticResponse.json")
    suspend fun getArticles(): Response<ArticleList>
}