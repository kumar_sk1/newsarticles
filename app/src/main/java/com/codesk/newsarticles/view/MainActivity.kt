package com.codesk.newsarticles.view

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.codesk.newsarticles.R
import com.codesk.newsarticles.adapter.ArticleListAdapter
import com.codesk.newsarticles.model.ArticleList
import com.codesk.newsarticles.utils.NetworkUtils
import com.codesk.newsarticles.viewmodel.ArticleViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.canonicalName
    private lateinit var mArticleViewModel: ArticleViewModel
    private lateinit var mArticleListAdapter: ArticleListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.inflateMenu(R.menu.sort_menu)
        isLoadingAnimationStart(true)
        mArticleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        mArticleViewModel.init()
        setupRecyclerView()
        observeViewModel(mArticleViewModel, mArticleListAdapter)

        if (!NetworkUtils.hasNetwork(this)) {
            shimmer_view_container.visibility = View.GONE
            if (mArticleViewModel.getArticleList().value == null) {
                errorView.visibility = View.VISIBLE
            }
        }

        // pull refresh listener for fetching latest data on user demand
        pullToRefresh.setOnRefreshListener {
            Log.d(TAG,"pullToRefresh setOnRefreshListener()")
            if (!NetworkUtils.hasNetwork(this)) {
                pullToRefresh.isRefreshing = false
                if (mArticleViewModel.getArticleList().value == null) {
                    errorView.visibility = View.VISIBLE
                }
            }else {
                errorView.visibility = View.INVISIBLE
                mArticleViewModel.init()          // update recycler view list with latest data
                isLoadingAnimationStart(true)
                pullToRefresh.isRefreshing = false
            }
        }

        toolbar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener { item: MenuItem? ->
            val articleList = mArticleViewModel.getArticleList().value?.articles!!
            when (item!!.itemId) {
                R.id.sortNewToOld -> {
                    Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    mArticleViewModel.getArticleList().value?.articles = articleList.sortedWith(compareBy({ it.source?.name }, { it.source?.name })).reversed()
                    mArticleListAdapter.notifyDataSetChanged()
                }
                R.id.sortOldToNew -> {
                    Toast.makeText(this@MainActivity, item.title, Toast.LENGTH_SHORT).show()
                    mArticleViewModel.getArticleList().value?.articles = articleList.sortedWith(compareBy({ it.source?.name }, { it.source?.name }))
                    mArticleListAdapter.notifyDataSetChanged()
                }
            }

            true
        })

    }

    private fun setupRecyclerView() {
        Log.d(TAG,"setupRecyclerView()")
        mArticleListAdapter = ArticleListAdapter(mArticleViewModel.getArticleList(), this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = mArticleListAdapter
        recycler_view.setItemViewCacheSize(25)
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.isNestedScrollingEnabled = true
    }

    private fun observeViewModel(mAboutViewModel: ArticleViewModel, mArticleListAdapter: ArticleListAdapter) {
        Log.d(TAG,"observeViewModel()")
        mAboutViewModel.getArticleList().observe(this, Observer<ArticleList>{
            Log.d(TAG,"notifyDataSetChanged()")
            errorView.visibility = View.INVISIBLE
            isLoadingAnimationStart(false)
            mArticleListAdapter.notifyDataSetChanged()
        })
    }

    private fun isLoadingAnimationStart(isVisible: Boolean) {
        if (isVisible) {
            shimmer_view_container.startShimmer()
            shimmer_view_container.visibility = View.VISIBLE
        } else {
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
        }
    }
}
