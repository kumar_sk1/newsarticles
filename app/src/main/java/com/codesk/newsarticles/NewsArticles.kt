package com.codesk.newsarticles

import android.app.Application
import android.content.Context


class NewsArticles : Application() {
    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    companion object {
        private lateinit var instance: NewsArticles
        val context: Context
            get() = instance
    }
}