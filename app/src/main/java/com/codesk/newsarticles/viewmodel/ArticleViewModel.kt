package com.codesk.newsarticles.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codesk.newsarticles.model.ArticleList
import com.codesk.newsarticles.restservice.RefAppServerProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class ArticleViewModel : ViewModel() {
    private val TAG = ArticleViewModel::class.java.canonicalName
    private var articleList: MutableLiveData<ArticleList> = MutableLiveData()

    fun getArticleList(): MutableLiveData<ArticleList> {
        Log.d(TAG, "getArticleList()")
        return articleList
    }

    fun init() {
        Log.d(TAG, "init() Called")
        CoroutineScope(Dispatchers.Default).launch {
            loadArticleList()
        }
    }

    private suspend fun loadArticleList() {
        Log.d(TAG, "loadArticleList()")
        try {
            val response = RefAppServerProvider.refApi.getArticles()
            if (response.isSuccessful) {
                CoroutineScope(Dispatchers.Main).launch {
                    articleList.value = response.body()
                }
            } else {
                Log.d(TAG, "Api Response failed")
            }
        } catch (ex: Exception) {
            print(ex)
        }
    }
}