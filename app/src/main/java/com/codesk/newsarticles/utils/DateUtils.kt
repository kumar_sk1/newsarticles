package com.codesk.newsarticles.utils

import com.codesk.newsarticles.NewsArticles
import com.codesk.newsarticles.R
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun convertUTCtoDate(publishedAt: String?): String {
        return try{
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy")
            val date: Date = inputFormat.parse(publishedAt)
            outputFormat.format(date)

        }catch (ex: Exception){
            NewsArticles.context.resources.getString(R.string.no_publishDate)
        }
    }
}